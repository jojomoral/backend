import flask, sqlite3, logging, json, hashlib, random, string
from flask import Flask, request, render_template, make_response, flash, session
from flask_httpauth import HTTPBasicAuth

import CONFIG

app = Flask(__name__)

app.config['SECRET_KEY'] = CONFIG.secret_key

auth = HTTPBasicAuth()

form_names = CONFIG.form_names

sql_conn = sqlite3.connect('db.db', check_same_thread=False)
sql_curs = sql_conn.cursor() 
 
@auth.verify_password
def verify_password(username, password):
	try:
		password = hashlib.sha256(password.encode('utf-8')).hexdigest()
		sql = f"SELECT * FROM admins WHERE username = '{username}' AND password = '{password}'"
		sql_curs.execute(sql)
		select = sql_curs.fetchall()
		sql_conn.commit()
		if len(select) == 0:
			return False
		else:
			return True	
	except Exception as ex:
		print(ex)
		

@app.route('/adminstatic', methods=['GET'])
@auth.login_required
def adminstatistic():
	return render_template('adminstatic.html', static = get_db_static())	
	
@app.route('/adminuserlist', methods=['GET'])
@auth.login_required
def adminuserlist():
	return render_template('adminuserlist.html', db = get_db_all())
	

@app.route('/adminuserlist/delete/<num>', methods=['GET'])
@auth.login_required
def delete(num):
	if db_delete_user(num):
		flash("Пользователь успешно удален", category='success')
	else:
		flash("Ошибка удаления пользователя", category='error')
	return flask.redirect(flask.url_for('adminuserlist'))
	
	
@app.route('/adminuserlist/change/<num>', methods=['GET', 'POST'])
@auth.login_required
def books(num):
	if request.method == 'GET':
		all = get_db_by_id(num)
		rememb = {}
		for i in range(len(form_names)):
			rememb[form_names[i]] = all[i]
		if not request.cookies.get('form_errors'):
		
			return render_template('adminchangedb.html', errors = '', answ = rememb, id = num)
		else: 
			error = request.cookies.get('form_errors').split(',')
			return render_template('adminchangedb.html', errors = error, answ = rememb, id = num)
				
	elif request.method == 'POST':
			session['id'] = int(num)
			error = []
			name = request.form.get("field-name")
			email = request.form.get("field-email")
			date = request.form.get("field-date")
			male = request.form.get("radio-group-1")
			conechn = request.form.get("radio-group-2")
			super = request.form.get("super")
			biography = request.form.get("biography")
			
			if name == '' or name == None:
				error.append('Не введено имя')
			if email == '' or email == None:
				error.append('Не введен email')
			if date == '' or date == None:
				error.append('Не указана дата')
			if male == '' or male == None:
				error.append('Не указан пол')
			if conechn == '' or conechn==None:
				error.append('Не указано количество конечностей')
			if super == '' or super == None:
				error.append( 'Не указана суперспособность')
			if biography == '' or biography == None:
				error.append( 'Не введена биография')
				
				
			if len(error) == 0:
				db_update_main(name, email, date, male, conechn,super,biography)
						
				res = make_response("")
				rememb = {}
				for key in request.form:
					item = request.form.get(key)
					if item != '' and item != None:
						rememb[key] = item

				res.set_cookie('form_errors','None', max_age = 0)
				res.set_cookie('form_remember', str(rememb), max_age = 31536000)
				
					
				res.headers['location'] = flask.url_for('adminuserlist')+'/change/'+f'{num}'
				flash("Форма успешно изменена", category='success')
				return res,302
				
			else:
				res = make_response("")
				rememb = {}
				for key in request.form:
					item = request.form.get(key)
					if item != '' and item != None:
						rememb[key] = item

				res.set_cookie('form_errors', ','.join(error))
				res.set_cookie('form_remember', str(rememb), max_age = 31536000)
				res.headers['location'] = flask.url_for('adminuserlist')+'/change/'+f'{num}'
				flash("Ошибка отправки формы", category='error')
				
				return res,302
				
				
@app.route('/admin', methods=['GET'])
@auth.login_required
def admin():
	return render_template('admin.html')

@app.route('/newform', methods=['GET'])
def newform():
	session.pop('is_get_login', None)
	session.pop('is_login', None)
	session.pop('id', None)
	return flask.redirect(flask.url_for('index'))

@app.route('/', methods=['GET'])
def index():
	session['page'] = 'index'
	if not request.cookies.get('form_errors'):
		if not request.cookies.get('form_remember'):
			return render_template('index.html', errors = '', answ = {})
		else:
			rememb =json.loads(request.cookies.get('form_remember').replace("'", '"'))
			return render_template('index.html', errors = '', answ = rememb)
	else: 
		error = request.cookies.get('form_errors').split(',')
		if not request.cookies.get('form_remember'):
			return render_template('index.html', errors = error, answ = {})
		else:
			rememb =json.loads(request.cookies.get('form_remember').replace("'", '"'))
			return render_template('index.html', errors = error, answ = rememb)

@app.route('/', methods=['POST'])
def form():
	if request.method == 'POST':
		try:
		
			error = []
			name = request.form.get("field-name")
			email = request.form.get("field-email")
			date = request.form.get("field-date")
			male = request.form.get("radio-group-1")
			conechn = request.form.get("radio-group-2")
			super = request.form.get("super")
			biography = request.form.get("biography")
			
			if name == '' or name == None:
				error.append('Не введено имя')
			if email == '' or email == None:
				error.append('Не введен email')
			if date == '' or date == None:
				error.append('Не указана дата')
			if male == '' or male == None:
				error.append('Не указан пол')
			if conechn == '' or conechn==None:
				error.append('Не указано количество конечностей')
			if super == '' or super == None:
				error.append( 'Не указана суперспособность')
			if biography == '' or biography == None:
				error.append( 'Не введена биография')
				
				
			if len(error) == 0:
				if 'id' not in session:
						db_insert_main(name, email, date, male, conechn,super,biography)
						session['id'] = db_get_last_id()
				else:
					db_update_main(name, email, date, male, conechn,super,biography)
						
				res = make_response("")
				rememb = {}
				for key in request.form:
					item = request.form.get(key)
					if item != '' and item != None:
						rememb[key] = item

				res.set_cookie('form_errors','None', max_age = 0)
				res.set_cookie('form_remember', str(rememb), max_age = 31536000)
				
				if 'is_get_login' not in session:
					login = generate_alphanum_random_string(10)
					password = generate_alphanum_random_string(10)
					flash(f'Логин: {login}')
					flash(f'Пароль: {password}')
					add_user_db(login, password)
					session['is_get_login'] = 'True'
					
				res.headers['location'] = flask.url_for('index')
				flash("Форма успешно отправлена", category='success')
				return res,302
				
			else:
				res = make_response("")
				rememb = {}
				for key in request.form:
					item = request.form.get(key)
					if item != '' and item != None:
						rememb[key] = item

				res.set_cookie('form_errors', ','.join(error))
				res.set_cookie('form_remember', str(rememb), max_age = 31536000)
				res.headers['location'] = flask.url_for('index')
				flash("Ошибка отправки формы", category='error')
				
				return res,302

		except Exception:
			logging.exception('')
			return ''

@app.route('/setlogin', methods=['GET'])		
def setlogin():
	session['is_get_login'] = 'True'
	session.pop('is_login', None)
	return flask.redirect(flask.url_for('login'))
	
@app.route('/login', methods=['GET', 'POST'])
def login():
	session['page'] = 'login'
	if request.method == 'GET':
	
		if not request.cookies.get('form_errors'):
			if not request.cookies.get('form_remember'):
				return render_template('index.html', errors = '', answ = {})
			else:
				rememb =json.loads(request.cookies.get('form_remember').replace("'", '"'))
				return render_template('index.html', errors = '', answ = rememb)
		else: 
			error = request.cookies.get('form_errors').split(',')
			if not request.cookies.get('form_remember'):
				return render_template('index.html', errors = error, answ = {})
			else:
				rememb =json.loads(request.cookies.get('form_remember').replace("'", '"'))
				return render_template('index.html', errors = error, answ = rememb)
				
				
	elif request.method == 'POST':
		res = make_response("")
		
		login = request.form.get("field-login")
		password = request.form.get("field-password")
		check, user = check_user(login, password)
		if check:
			flash("Вы успешно вошли", category='success')
			session['is_login'] = 'True'
			session['id'] = int(user[0])
			rememb = {}
			for i in range(len(form_names)):
				rememb[form_names[i]] = user[i+1]			
			res.set_cookie('form_remember', str(rememb), max_age = 31536000)
	
		else:
			flash("Неправильный логин или пароль", category='error')
			
		res.headers['location'] = flask.url_for('login')
		return res, 302
		

	
def check_user(login, password):
	try:
		password = hashlib.sha256(password.encode('utf-8')).hexdigest()
		sql = f"SELECT * FROM main WHERE login = '{login}' AND password = '{password}'"
		sql_curs.execute(sql)
		select = sql_curs.fetchall()
		sql_conn.commit()
		if len(select) == 0:
			return [False, 0]
		else:
			return [True, select[0]]			
	except Exception as ex:
		print(ex)


def db_insert_main(name, email, date, male, conechn,super,biography):
	sql = f"INSERT INTO main (name, email, date, male, conechn,super,biography) VALUES ('{name}', '{email}', '{date}', '{male}', '{conechn}', '{super}', '{biography}')"
	sql_curs.execute(sql)
	sql_conn.commit()	

def db_get_last_id():
	sql = f"SELECT id FROM main WHERE id=last_insert_rowid()"
	sql_curs.execute(sql)
	select = sql_curs.fetchall()
	sql_conn.commit()
	for row in select:
		return row[0]

def db_update_main(name, email, date, male, conechn,super,biography):
	try:
		sql = f"UPDATE main SET name = '{name}', email = '{email}', date = '{date}', male = '{male}', conechn = '{conechn}' ,super = '{super}', biography = '{biography}' WHERE id = {int(session['id'])}"
		sql_curs.execute(sql)
		sql_conn.commit()
	except Exception as ex:
		print(ex)
def get_db_all():
	try:
		sql = f"SELECT * FROM main"
		sql_curs.execute(sql)
		select = sql_curs.fetchall()
		sql_conn.commit()
		return select
	except Exception as ex:
		print(ex)
		
def get_db_by_id(id):
	try:
		sql = f"SELECT name, email, date, male, conechn,super,biography FROM main WHERE id={id}"
		sql_curs.execute(sql)
		select = sql_curs.fetchall()
		sql_conn.commit()
		return select[0]
	except Exception as ex:
		print(ex)
		
def add_user_db(login, password):
	try:
		password = hashlib.sha256(password.encode('utf-8')).hexdigest()
		sql = f"UPDATE main SET login = '{login}', password = '{password}' WHERE id={int(session['id'])}"
		sql_curs.execute(sql)
		sql_conn.commit()					
	except Exception as ex:
		print(ex)
		
def db_delete_user(id):
	try:
		sql = f"DELETE FROM main WHERE id = {id}"
		sql_curs.execute(sql)
		sql_conn.commit()
		return True
	except Exception as ex:
		return False
		print(ex)

def get_db_static():
	try:
		static = []
		sql = "SELECT COUNT(*) FROM main WHERE super='бессмертие'"
		sql_curs.execute(sql)
		select = sql_curs.fetchall()
		sql_conn.commit()
		static.append(select[0][0])
		sql = "SELECT COUNT(*) FROM main WHERE super='прохождение сквозь стены'"
		sql_curs.execute(sql)
		select = sql_curs.fetchall()
		sql_conn.commit()
		static.append(select[0][0])
		sql = "SELECT COUNT(*) FROM main WHERE super='левитация'"
		sql_curs.execute(sql)
		select = sql_curs.fetchall()
		sql_conn.commit()
		static.append(select[0][0])
		sql = "SELECT COUNT(*) FROM main WHERE super='сдавать сессию на 5'"
		sql_curs.execute(sql)
		select = sql_curs.fetchall()
		sql_conn.commit()
		static.append(select[0][0])
		return static
	except Exception as ex:
		print(ex)

def generate_alphanum_random_string(length):
    letters_and_digits = string.ascii_letters + string.digits
    rand_string = ''.join(random.sample(letters_and_digits, length))
    return rand_string.lower()    		 
		 
if __name__ == '__main__':
    app.run(port=8000, host='194.87.101.70')
